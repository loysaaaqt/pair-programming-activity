import java.util.Scanner;

public class Main {
    static Scanner input = new Scanner(System.in);
    static int sold = 0; 
    static int sold1 = 0;
    static int sold2 = 0;
    public static void main(String[] args) {
        System.out.println("\n*****************************");
        System.out.println("ELECTRONIC BOOK RENTAL SYSTEM");
        System.out.println("*****************************");
        
        Books[] arr = new Books[3];

        arr[0] = new Books("System Analysis and Design,", "Gary B.Shelly,", 1998, 2, sold);
        arr[1] = new Books("Android Application,", "Corrine Hoisington,", 2000, 3, sold1);
        arr[2] = new Books("Programming Concepts and Logic Formulation,", "Rosauro E. Manuel,", 1981, 4, sold2);

        System.out.print("0 ");
        arr[0].display();
  
        System.out.print("1 ");
        arr[1].display();
        
        System.out.print("2 ");
        arr[2].display();
        System.out.println("*****************************");

        System.out.println("CHOOSE A NUMBER TO RENT A BOOK:");
	Scanner input = new Scanner(System.in);
	int sc = input.nextInt();

	switch (sc) {
	case 0:
            
            if(sold == 2){
                System.out.println("No Copies Available");
                
           break;
            }
            else if (sold < 2){
                
            book1();
            }
		break;
                
	case 1:
            
            if(sold1 == 3){
                System.out.println("No Copies Available");
                
           break;
            }
            else if (sold1 < 3){
                
            book2();
            }
		break;
	case 2:
            
            if(sold2 == 4){
                System.out.println("No Copies Available");
                
           break;
            }
            else if (sold2 < 4){
            book3();
            }
		break;
                
	default:
		 System.out.println("INDEX DOES NOT EXIST. Try Again!");
                 System.exit(0);
	}
       
}
    
    private static void book1() {  
        Books[] arr = new Books[3];

        arr[0] = new Books("System Analysis and Design", "Gary B.Shelly,", 1998, 2, sold);
        
        System.out.print("You Rented ");
        System.out.println(arr[0].title);
 
        Scanner userInput = new Scanner(System.in);
        boolean noSelection = true;
        do {
            System.out.println("RENT AGAIN? Y/N");
 
            char userChoice = userInput.next().charAt(0);
 
            switch (userChoice) {
                case 'Y':
                    sold += 1;
                    main(null); 
                    break;
 
                case 'N':
                    noSelection = false;
                    break;
 
                default:
                    System.out.println("That is not a valid entry");
 
            }
 
        }  while (noSelection);
 
        System.out.println("PROGRAM ENDS");
        System.exit(0);
    }

    
    private static void book2() {
        Books[] arr = new Books[3];

        arr[1] = new Books("Android Application", "Corrine Hoisington,", 2000, 3, sold1);
        
        System.out.print("You Rented ");
        System.out.println(arr[1].title);
         
        Scanner userInput = new Scanner(System.in);
        boolean noSelection = true;
        do {
            System.out.println("RENT AGAIN? Y/N");
 
            char userChoice = userInput.next().charAt(0);
 
            switch (userChoice) {
                case 'Y':
                    sold1 += 1; 
                    main(null);
                    break;
 
                case 'N':
                    noSelection = false;
                    break;
 
                default:
                    System.out.println("That is not a valid entry");
 
            }
 
        }  while (noSelection);
 
        System.out.println("PROGRAM ENDS");
        System.exit(0);
    }
    
    private static void book3() {
        Books[] arr = new Books[3];

        arr[2] = new Books("Programming Concepts and Logic Formulation", "Rosauro E. Manuel,", 1981, 4, sold2);
        
        System.out.print("You Rented ");
        System.out.println(arr[2].title);
         
        Scanner userInput = new Scanner(System.in);
        boolean noSelection = true;
        do {
            System.out.println("RENT AGAIN? Y/N");
 
            char userChoice = userInput.next().charAt(0);
 
            switch (userChoice) {
                case 'Y':
                    sold2 += 1;
                    main(null);
                    break;
 
                case 'N':
                    noSelection = false;
                    break;
 
                default:
                    System.out.println("That is not a valid entry");
 
            }
 
        }  while (noSelection);
 
        System.out.println("PROGRAM ENDS");
        System.exit(0);
    }
}