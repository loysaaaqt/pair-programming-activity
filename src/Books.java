public class Books {
    String title;
    private String author;
    private int year;
    int copies;
    private int sold; 
    
    Books(String title, String author, int year, int copies, int sold)
    {
        this.title = title;
        this.author = author;
        this.year = year;
        this.copies = copies;
        this.sold = sold; 
    }
        
    public String getTitle(){
    return title;
    }
    
    public void setTitle(String title){
        
    }
    
   public String getAuthor(){
    return author;
    }
    
    public void setAuthor(String author){

    }
    
    public int getYear(){
    return year;
    }
    
    public void setYear(int year){

    }
    
    public int getCopies(){    
    return copies - sold;              
    }
    
    public void setCopies(int copies){               
    
    }
    
    public int getSold(){ 
    return sold;
    }
   
    public void setSold(int sold){               
    
    }
  
    public void display()
    {
        System.out.println(title + " " + author + " " + year);
        System.out.println();
            
    }               
}
